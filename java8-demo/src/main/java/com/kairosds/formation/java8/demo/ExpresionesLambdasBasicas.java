
package com.kairosds.formation.java8.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class ExpresionesLambdasBasicas {

    private void ejecutar() {
        System.out.println("Expresiones Lambdas Básicas");

        System.out.println("Ejecutando ejercicio 1");
        ejercicio1();

        System.out.println("Ejecutando ejercicio 4");
        ejercicio4();

        System.out.println("Ejecutando ejercicio 5");
        ejercicio5();

        System.out.println("Ejecutando ejercicio 6");
        ejercicio6();

    }


    private void ejercicio1() {
        printTeams(new ArrayList<>(), new CheckTeam() {

            // Crear función anónima para imprimir el valor de aquellos equipos
            // que hayan ganado series del mundo entre 1990 y 2015
            @Override
            public boolean test(Team t) {
                return t.isHasWonWoldSeries() && t.getLastTimeWonWorldSeries() >= 1990 && t
                        .getLastTimeWonWorldSeries() <= 2015;
            }
        });

    }

    private void ejercicio4() {

        // Crear la interfaz funcional de tipo predicado para imprimir el valor de aquellos equipos
        // que hayan ganado series del mundo entre 1990 y 2015
        printTeamsWithPredicate(new ArrayList<>(),
                                t -> t.isHasWonWoldSeries() && t.getLastTimeWonWorldSeries() >= 1990 && t
                                        .getLastTimeWonWorldSeries() <= 2015);
    }

    private void ejercicio5() {

        // Para una lista de Teams, Usando un predicate, consumer y una función:
        // Filtrar por los equipos que ganaron series entre 1990 y 2015, pasarle en la función el equipo y devolver
        // su id, y el consumidor imprimir el valor obtenido
        processTeamsWithFunction(new ArrayList<>(),
                                 t -> t.isHasWonWoldSeries() && t.getLastTimeWonWorldSeries() >= 1990 && t
                                         .getLastTimeWonWorldSeries() <= 2015, t -> t.getTeamId(),
                                 name -> System.out.println(name));
    }

    private void ejercicio6() {

        List<Team> teams = new ArrayList<>();

        // Usando stream, filtrar por los equipos que han ganado series entre 1990 y 2015, imprimiendo el valor de
        // cada uno de ellos
        teams.stream().filter(t -> t.isHasWonWoldSeries() && t.getLastTimeWonWorldSeries() >= 1990 && t
                .getLastTimeWonWorldSeries() <= 2015).forEach((Team t) -> {
            System.out.println(" " + t.getName());
        });


    }


    public static void printTeams(List<Team> teams, CheckTeam tester) {

        for (Team t : teams) {
            if (tester.test(t)) {
                System.out.println("" + t.toString());
            }
        }
    }

    public static void printTeamsWithPredicate(List<Team> teams, Predicate<Team> tester) {

        for (Team t : teams) {
            if (tester.test(t)) {
                System.out.println("" + t.toString());
            }
        }
    }


    public static void processTeamsWithFunction(List<Team> teams, Predicate<Team> tester, Function<Team, String> mapper,
            Consumer<String> block) {
        for (Team t : teams) {
            if (tester.test(t)) {
                String data = mapper.apply(t);
                block.accept(data);
            }
        }
    }

    public static void main(String[] args) {

        ExpresionesLambdasBasicas exp = new ExpresionesLambdasBasicas();
        exp.ejecutar();

    }

}
